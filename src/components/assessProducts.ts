import { State } from "@/GlobalProvider";
import { inject, ref, ToRefs } from "vue";

export interface AssessProductsRequest {
  questionCode: string;
  firstProduct: string;
  secondProduct: string;
  answer: number;
}

export const useAssessProductsApi = (state: ToRefs<State>) => {
  const isLoading = ref(false);
  const error = ref(null);

  const assessProducts = async (req: AssessProductsRequest) => {
    isLoading.value = true;

    const { baseUrl, userId } = state;
    const url = `${baseUrl.value}zcsm_assessment_api/assess?id=${userId.value}`;

    try {
      const response = await (
        await fetch(url, {
          method: "POST",
          body: JSON.stringify({ id: userId.value, ...req }),
        })
      ).json();
    } catch (e) {
      error.value = e;
    }
    isLoading.value = false;
  };

  return { isLoading, error, assessProducts };
};
