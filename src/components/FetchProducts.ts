import { GlobalStateSymbol, State } from "@/GlobalProvider";
import { inject, ref, ToRefs } from "vue";

export interface Product {
  questionCode: string;
  questionText: string;
  firstProduct: string;
  firstProductText: string;
  firstProductImageUrl: string;
  secondProduct: string;
  secondProductText: string;
  secondProductImageUrl: string;
  firstAnswerCode: number;
  firstAnswerIcon: String;
  firstAnswerColor: string;
  secondAnswerCode: number;
  secondAnswerIcon: string;
  secondAnswerColor: string;
}
type Products = Array<Product>;

export const useFetchProductsApi = () => {
  const isLoading = ref(false);
  const error = ref(null);
  const data = ref<Products | null>(null);
  const globalState = inject<ToRefs<State>>(GlobalStateSymbol)!;
  const { baseUrl, userId } = globalState;

  const fetchProducts = async () => {
    isLoading.value = true;

    const url = `${baseUrl.value}zcsm_assessment_api/assess?id=${userId.value}`;

    try {
      const response = await (await fetch(url)).json();
      data.value = response.data as Products;
    } catch (e) {
      error.value = e;
    }
    isLoading.value = false;
  };

  return { isLoading, error, data, fetchProducts, globalState };
};
