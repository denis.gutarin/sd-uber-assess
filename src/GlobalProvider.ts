import { defineComponent, provide, reactive, readonly, toRefs } from "vue";

// We use symbols as unique identifiers.
export const GlobalStateSymbol = Symbol(
  "User settings state provider identifier"
);
export const GlobalStateUpdateSymbol = Symbol(
  "User settings update provider identifier"
);

export interface State {
  userId: string;
  baseUrl: string;
  [key: string]: any;
}

export default defineComponent({
  setup() {
    const params = new URLSearchParams(window.location.search);
    const hostname =
      window.location.hostname === "localhost"
        ? "dev.pas.sdvor.com"
        : window.location.hostname;
    const state: State = reactive({
      userId: params.get("userId") ?? "",
      baseUrl: `https://${hostname}/api/common/`,
    });
    provide(GlobalStateSymbol, toRefs(readonly(state)));

    const update = (property: string, value: string) => {
      state[property] = value;
    };
    provide(GlobalStateUpdateSymbol, update);
  },
  render() {
    return this!.$slots!.default!();
  },
});
