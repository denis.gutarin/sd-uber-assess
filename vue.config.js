
module.exports = {
    publicPath: './',
    configureWebpack: {
      optimization: {
        splitChunks: false
      }
    },
    css: {
      extract: false,
    },
    chainWebpack(config) {
        config.optimization.minimizer("terser").tap(args => {                              
          args[0].extractComments = true
          return args
        })
      }
  }